const express = require('express');
const keys = require('./keys/keys');
const bodyParser = require('body-parser');
const getVenues = require('./modules/getVenues');

//initialize
var app = express();
//set engine
app.set('view engine', 'ejs');

var city = 'Viterbo, VT';  //city to research query es. 'roma', 'via garibaldi 4, siena, italia'
var lang = 'it'; //language of research city (it, en, jp, fr... )
var n_items = '7'; //number of items shown up to 10
var search = 'gelateria'; //search query es. 'bar', 'ristorante', ....

app.get('/', (req,res)=>{
  getVenues(search,city,lang,n_items, (result)=>{
      res.send(JSON.parse(result));
  });
});

app.listen(3000, ()=>{
  console.log('Listening on port 3000');
});
