ADD file keys.js with 

var keys = {
  fs: {
    id : 'Your foursquare ID'
    secret : 'Your foursquare SECRET'
  },
  google: {
    apiKey: 'Your google api key'
  }
};

module.exports = keys;
