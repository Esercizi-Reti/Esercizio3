const request = require('request');
const getCoords = require('./getCoords');
const keys = require('../keys/keys');

//get latitude and longitude
module.exports = function(search,city,lang,n_items,callback){  // bar, Roma, it, 5
  getCoords(city, lang, (result)=>{  // get geo-coord by google maps
  //console.log('lat result = '+JSON.stringify(result));
  request({
      url: 'https://api.foursquare.com/v2/venues/explore',  //GET /explore?client_id=123&client_secret=456... HTTP/1.x
      method: 'GET',
      qs: {
        client_id: keys.fs.id,
        client_secret: keys.fs.secret,
        ll: result.lat+','+result.lng,
        query: search,
        v: '20180323',  // foursquare version year, vs='YYYYMMDD'
        limit: n_items  //n° of items
        }
      },(err, res, body)=>{
        if (err)
          console.log(err)
        else{
          //console.log('StatusCode: '+res.statusCode + '\nStatusMessage: ' +res.statusMessage);
          //console.log(body);
          callback(body);
        }
      });
    });
};
