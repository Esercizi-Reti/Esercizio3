var googleMaps = require('@google/maps');
const keys = require('../keys/keys');

//setup apiKey
googleMapsClient = googleMaps.createClient({
  key : keys.google.apiKey
});

//getting coords
module.exports = function(address, lang, callback){  // Roma, it
  //console.log(
    googleMapsClient.geocode({
      address : address,
      language : lang
      }, (err, res)=>{
        if(!err){
          // console.log('log google: '+JSON.stringify(res.json.results[0].formatted_address));
          // console.log('log google: '+JSON.stringify(res.json.results[0].geometry.location));
          callback(res.json.results[0].geometry.location);
          return;
        }
    });
    //.finally.toString());
  };
